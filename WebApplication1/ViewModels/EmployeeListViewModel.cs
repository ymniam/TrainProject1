﻿using System;
using System.Collections.Generic;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.ViewModels
{
    public class EmployeeListViewModel : BaseViewModel
    {
        public List<EmployeeViewModel> Employees { get; set;  }
    }
}
